#include "veinprotocolwrapper.h"

#include <veincore.pb.h>

#include <QDebug>

VeinProtocolWrapper::VeinProtocolWrapper()
{
}

VeinProtocolWrapper::~VeinProtocolWrapper()
{
}

google::protobuf::Message *VeinProtocolWrapper::byteArrayToProtobuf(QByteArray bA)
{
  protobuf::VeinProtocol *proto = new protobuf::VeinProtocol();
  if(!proto->ParseFromArray(bA, bA.size()))
  {
    qCritical() << "Error parsing protobuf:\n" << bA.toBase64();
    Q_ASSERT(false);
  }
  return proto;
}

QByteArray VeinProtocolWrapper::protobufToByteArray(google::protobuf::Message *pMessage)
{
  return QByteArray(pMessage->SerializeAsString().c_str(), pMessage->ByteSize());
}

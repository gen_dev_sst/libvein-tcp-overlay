#ifndef VEINTCPCONTROLLER_H
#define VEINTCPCONTROLLER_H

#include <QObject>
#include <QHash>
#include <QUuid>

#include <QtCore/qglobal.h>

#if defined(LIBVEINTCPOVERLAY_LIBRARY)
#  define LIBVEINTCPOVERLAYSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LIBVEINTCPOVERLAYSHARED_EXPORT Q_DECL_IMPORT
#endif

class VeinProtocolWrapper;
class VeinHub;
class VeinPeer;
class ProtoNetServer;
class ProtoNetPeer;

namespace google
{
  namespace protobuf
  {
    class Message;
  }
}

class LIBVEINTCPOVERLAYSHARED_EXPORT VeinTcpController : public QObject
{
  Q_OBJECT
public:
  explicit VeinTcpController(QObject *qObjParent=0);
  ~VeinTcpController();
  void startService(quint16 port);

  VeinHub *getLocalHub();
  QList<ProtoNetPeer *> getPeerlist();
  VeinHub *getRemoteHubFromConnection(ProtoNetPeer *pPeer);
  ProtoNetPeer *getRemoteConnectionFromHub(VeinHub *vHub);

  ProtoNetServer *getServerInstance();

signals:
  void sigHubCreated(VeinHub *vHub);
  void sigPeerAvailable(VeinPeer *vPeer);
  void sigClientConnected(ProtoNetPeer *pPeer);

public slots:
  void broadcastMessage(google::protobuf::Message *pMessage);
  ProtoNetPeer *connectToServer(QString host, quint16 port);
  void onClientConnected(ProtoNetPeer *pPeer);
  void onClientConnectionEstablished();
  void onClientDisconnected();
  void onLocalHubSendMessage(google::protobuf::Message *pMessage);
  void onMessageReceived(google::protobuf::Message *pMessage);
  void onSocketError();

  bool unicastMessage(const QUuid &receiver, google::protobuf::Message *pMessage);

protected slots:
  void onHubSendMessage(google::protobuf::Message *pMessage);

private:
  // PIMPL

  VeinHub *localHub;
  QHash<QUuid, VeinHub *> hubTable;

  ProtoNetServer *serverInstance;

  VeinProtocolWrapper* veinProtoWrapper;

  QHash<QUuid, ProtoNetPeer *> peerTable;

};

#endif // VEINTCPCONTROLLER_H

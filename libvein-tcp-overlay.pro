#-------------------------------------------------
#
# Project created by QtCreator 2013-11-18T10:50:30
#
#-------------------------------------------------
include(../include/project-paths.pri)

QT       += network

QT       -= gui

TARGET = vein-tcp-overlay
TEMPLATE = lib

DEFINES += LIBVEINTCPOVERLAY_LIBRARY

SOURCES += veintcpcontroller.cpp \
    veinprotocolwrapper.cpp

PUBLIC_HEADERS = veintcpcontroller.h

HEADERS += $$PUBLIC_HEADERS \
    veinprotocolwrapper.h


QMAKE_CXXFLAGS += -Wall -Wshadow


INCLUDEPATH += $$VEIN_INCLUDEDIR
LIBS += $$VEIN_LIBDIR -lvein-qt
INCLUDEPATH += $$PROTONET_INCLUDEDIR
LIBS += $$PROTONET_LIBDIR -lproto-net-qt
INCLUDEPATH += $$VEIN_PROTOBUF_INLCUDEDIR
LIBS += $$VEIN_PROTOBUF_LIBDIR -lvein-qt-protobuf

LIBS += -lprotobuf


target.path = /usr/lib

header_files.files = $$PUBLIC_HEADERS
header_files.path = /usr/include
INSTALLS += header_files
INSTALLS += target


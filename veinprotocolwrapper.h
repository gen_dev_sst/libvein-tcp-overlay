#ifndef VEINPROTOCOLWRAPPER_H
#define VEINPROTOCOLWRAPPER_H

#include <protonetwrapper.h>

class VeinProtocolWrapper : public ProtoNetWrapper
{
public:
  VeinProtocolWrapper();
  ~VeinProtocolWrapper();

  google::protobuf::Message *byteArrayToProtobuf(QByteArray bA) Q_DECL_OVERRIDE;

  QByteArray protobufToByteArray(google::protobuf::Message *pMessage) Q_DECL_OVERRIDE;

};

#endif // VEINPROTOCOLWRAPPER_H

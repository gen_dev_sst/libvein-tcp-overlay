#include "veintcpcontroller.h"

#include "veinprotocolwrapper.h"

#include <veinhub.h>
#include <veinpeer.h>
#include <protonetserver.h>
#include <protonetpeer.h>

#include <google/protobuf/message.h>
#include <veincore.pb.h>

VeinTcpController::VeinTcpController(QObject *qObjParent) :
  QObject(qObjParent),
  localHub(new VeinHub(this)),
  serverInstance(new ProtoNetServer(this)),
  veinProtoWrapper(new VeinProtocolWrapper())
{
  serverInstance->setDefaultWrapper(veinProtoWrapper);
  connect(serverInstance, &ProtoNetServer::sigClientConnected, this, &VeinTcpController::onClientConnected);
  connect(localHub, &VeinHub::sigSendProtobufMessage, this, &VeinTcpController::onLocalHubSendMessage);
}

VeinTcpController::~VeinTcpController()
{
  //empty dtor and parent class dtor, ignore this warning
  delete veinProtoWrapper;
}

void VeinTcpController::startService(quint16 port)
{
  connect(serverInstance, SIGNAL(sigClientConnected(ProtoNetPeer*)), this, SIGNAL(sigClientConnected(ProtoNetPeer*)));
  serverInstance->startServer(port);
}

VeinHub *VeinTcpController::getLocalHub()
{
  return localHub;
}

QList<ProtoNetPeer *> VeinTcpController::getPeerlist()
{
  return peerTable.values();
}

VeinHub *VeinTcpController::getRemoteHubFromConnection(ProtoNetPeer *pPeer)
{
  return hubTable.value(pPeer->getIdentityUuid(), 0);
}

ProtoNetPeer *VeinTcpController::getRemoteConnectionFromHub(VeinHub *vHub)
{
  return peerTable.value(vHub->getUuid(),0);
}

ProtoNetServer *VeinTcpController::getServerInstance()
{
  return serverInstance;
}

void VeinTcpController::broadcastMessage(google::protobuf::Message *pMessage)
{
  foreach(ProtoNetPeer *p, peerTable.values())
  {
    p->sendMessage(pMessage);
  }
}

ProtoNetPeer *VeinTcpController::connectToServer(QString host, quint16 port)
{
  ProtoNetPeer *tmpPeer = new ProtoNetPeer(this);
  connect(tmpPeer, SIGNAL(sigSocketError(QAbstractSocket::SocketError)), this, SLOT(onSocketError()));
  connect(tmpPeer, &ProtoNetPeer::sigConnectionEstablished, this, &VeinTcpController::onClientConnectionEstablished);
  tmpPeer->startConnection(host, port);

  return tmpPeer;
}

void VeinTcpController::onClientConnected(ProtoNetPeer *pPeer)
{
  connect(pPeer, &ProtoNetPeer::sigMessageReceived, this, &VeinTcpController::onMessageReceived);
  pPeer->setWrapper(veinProtoWrapper);
  google::protobuf::Message *tmpMessage = localHub->protobufHubInit();
  pPeer->sendMessage(tmpMessage);
  delete tmpMessage;
}

void VeinTcpController::onClientConnectionEstablished()
{
  ProtoNetPeer *tmpPeer = qobject_cast<ProtoNetPeer *>(QObject::sender());
  if(tmpPeer)
  {
    onClientConnected(tmpPeer);
  }
}



void VeinTcpController::onClientDisconnected()
{
  ProtoNetPeer * tmpPPeer = qobject_cast<ProtoNetPeer *>(QObject::sender());

  if(tmpPPeer)
  {
    VeinHub *tmpHub = hubTable.value(tmpPPeer->getIdentityUuid(),0);

    if(tmpHub)
    {
      hubTable.remove(tmpPPeer->getIdentityUuid());
      tmpHub->deleteLater();      
    }
    else
    {
      qDebug() << "[vein-tcp-overlay] No hub found for client:" << tmpPPeer->getIdentityUuid();
    }
    peerTable.remove(tmpPPeer->getIdentityUuid());
    tmpPPeer->deleteLater();
  }
}

void VeinTcpController::onLocalHubSendMessage(google::protobuf::Message *pMessage)
{
  if(pMessage)
  {
    protobuf::VeinProtocol *proto = static_cast<protobuf::VeinProtocol *>(pMessage);
    if(proto->has_command())
    {
      protobuf::Vein_Command *vCmd = proto->mutable_command();
      for(int i=0; i<vCmd->uidreceiverlist_size(); i++)
      {
        QUuid tmpUuid = QUuid::fromRfc4122(QByteArray(vCmd->uidreceiverlist(i).c_str(),vCmd->uidreceiverlist(i).size()));
        bool found = false;
        if(tmpUuid!=localHub->getUuid())
        {
          if(unicastMessage(tmpUuid, proto))
          {
            found = true;
          }
          else
          {
            qDebug() << "E12 Receiver not found:"<<tmpUuid;
            qDebug() << proto->DebugString().c_str();
          }
        }
        else
        {
          //this is a broadcast message
          //qDebug() << "W11 Unicast failed, Broadcasting for uuid:\n" << tmpUuid << "\nlocal uuid:\n" <<localHub->getUuid();
          broadcastMessage(proto);

          found = true;
        }
        if(found == false)
        {
          qDebug() << "E1 Could not find the receiver for message sent by localHub:" << tmpUuid;
        }
      }
    }
  }
}

void VeinTcpController::onMessageReceived(google::protobuf::Message *pMessage)
{
  ProtoNetPeer *tmpPPeer = qobject_cast<ProtoNetPeer *>(QObject::sender());
  if(tmpPPeer != 0 && pMessage != 0)
  {
    protobuf::VeinProtocol *proto = static_cast<protobuf::VeinProtocol *>(pMessage);
    if(proto && proto->has_command())
    {
      protobuf::Vein_Command *vCmd = proto->mutable_command();
      //if(vCmd->type() == protobuf::Vein_Command::REQUEST_ENTITY_START_ASYNC_UPDATE)
      //  qDebug() << "command 102 received";

      if(vCmd->type() == protobuf::Vein_Command::INIT_HUB)
      {
        VeinHub *tmpHub = new VeinHub(this);

        connect(tmpHub, &VeinHub::sigSendProtobufMessage, tmpPPeer, &ProtoNetPeer::sendMessage);
        connect(tmpHub, &VeinHub::sigPeerRegistered, this, &VeinTcpController::sigPeerAvailable);
        //connect(tmpHub, &VeinHub::sigSendProtobufMessage, this, &VeinTcpController::onHubSendMessage);

        tmpHub->initHub(proto);

        hubTable.insert(tmpHub->getUuid(),tmpHub);
        tmpPPeer->setIdentityUuid(tmpHub->getUuid());
        peerTable.insert(tmpHub->getUuid(), tmpPPeer);
        connect(tmpPPeer, &ProtoNetPeer::sigConnectionClosed, this, &VeinTcpController::onClientDisconnected);
        //qDebug() << peerTable;



        sigHubCreated(tmpHub);
      }
      else
      {
        for(int i=0; i<vCmd->uidreceiverlist_size(); i++)
        {
          QUuid tmpUuid = QUuid::fromRfc4122(QByteArray(vCmd->uidreceiverlist(i).c_str(),vCmd->uidreceiverlist(i).size()));
          VeinHub *tmpHub = hubTable.value(tmpUuid,0);
          if(tmpHub)
          {
            tmpHub->onProtobufMessageReceived(proto);
          }
          else
          {
            if(tmpUuid==localHub->getUuid())
            {
              localHub->onProtobufMessageReceived(proto);
            }
            else
            {
              qDebug() << "E2 Hub not found:" << tmpUuid;
            }
          }
        }
      }
    }
  }
}

void VeinTcpController::onSocketError()
{
  qDebug() << "Socket error";
}

bool VeinTcpController::unicastMessage(const QUuid &receiver, google::protobuf::Message *pMessage)
{
  bool retVal = false;
  ProtoNetPeer *tmpPPeer = peerTable.value(receiver,0);
  if(tmpPPeer != 0 && pMessage != 0)
  {
    tmpPPeer->sendMessage(pMessage);
    retVal = true;
  }
  // if the receiver is the localHub this is a broadcast to all listeners
  //else if(receiver == localHub->getUuid())
  return retVal;
}

void VeinTcpController::onHubSendMessage(google::protobuf::Message *pMessage)
{
  VeinHub *tmpHub = qobject_cast<VeinHub *>(QObject::sender());
  if(tmpHub)
  {
    if(!unicastMessage(tmpHub->getUuid(),pMessage))
    {
      //qDebug() << "13 Unicast failed for hub:"<<tmpHub->getUuid()<< "’\nmessage:\n"<<pMessage->DebugString().c_str();
    }
  }
}


